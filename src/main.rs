use anyhow::{bail, Result};
use clap::{crate_version, Parser};
use rand::Rng;

// EXT_ASCII = ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ
const EXT_ASCII: &[u8] = &[
    161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198,
    199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217,
    218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236,
    237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
];
const DIGIT: &[u8] = b"0123456789";
const LOWERCASE: &[u8] = b"abcdefghijklmnopqrstuvwxyz";
const SYMBOL: &[u8] = b"(*|)_+!>^@']/,#\"$&% `:[<\\;=-.{~}?";
const UPPERCASE: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const PASSWORD_LEN: u32 = 32;
const PASSWORD_NUM: u32 = 1;

#[derive(Parser)]
#[clap(version = crate_version!(), about = "Secure Password Generator")]
struct Opts {
    /// Set custom symbols
    #[clap(short, long, value_name = "SYMBOLS")]
    custom: Option<String>,

    /// Remove all symbols
    #[clap(short, long)]
    insecure: bool,

    /// Add extended ASCII characters
    #[clap(short = 'x', long)]
    extended: bool,

    /// Use (super) secure passwords
    #[clap(short, long, parse(from_occurrences))]
    secure: u8,

    /// Set password length
    length: Option<u32>,

    /// Number of passwords to print
    #[clap(short, long)]
    number: Option<u32>,

    /// Remove desired symbols
    #[clap(short, long, value_name = "SYMBOLS")]
    remove: Option<String>,
}

struct Pool<'a> {
    chars: Vec<char>,
    types: Vec<&'a [u8]>,
}

impl<'a> Pool<'a> {
    fn new() -> Pool<'a> {
        Pool {
            chars: Vec::new(),
            types: Vec::new(),
        }
    }
}

fn add_to_pool<'a>(pool: &mut Pool<'a>, slice: &'a [u8]) {
    pool.chars.extend(slice.iter().map(|item| *item as char));
    pool.types.push(slice);
}

fn generate_pool(opts: &Opts) -> Pool {
    let mut pool = Pool::new();

    add_to_pool(&mut pool, DIGIT);
    add_to_pool(&mut pool, UPPERCASE);
    add_to_pool(&mut pool, LOWERCASE);

    let custom = match &opts.custom {
        Some(s) => s,
        None => "",
    };
    if !custom.is_empty() {
        add_to_pool(&mut pool, custom.as_bytes());
    } else if opts.insecure {
    } else {
        add_to_pool(&mut pool, SYMBOL);
    }

    if opts.extended {
        add_to_pool(&mut pool, EXT_ASCII);
    }

    if let Some(remove_value) = &opts.remove {
        for remove in remove_value.chars() {
            for (i, elem) in pool.chars.iter().enumerate() {
                if *elem == remove {
                    pool.chars.remove(i);
                    break;
                }
            }
        }
    }
    pool
}

fn is_valid(string: &str, types: &[&[u8]]) -> bool {
    let mut valid: Vec<bool> = vec![];
    for t in types {
        let mut temp: bool = false;
        'char: for c in string.bytes() {
            if t.contains(&c) {
                temp = true;
                break 'char;
            }
        }
        if temp {
            valid.push(true);
        }
    }

    valid.iter().all(|x| *x) && valid.len() == types.len()
}

fn no_outer_spaces(string: &str) -> bool {
    !string.starts_with(' ') && !string.ends_with(' ')
}

fn main() -> Result<()> {
    let mut opts = Opts::parse();
    let mut rng = rand::thread_rng();

    if opts.secure > 0 {
        opts.length = Some(128);
        if opts.secure > 1 {
            opts.extended = true;
        }
    }

    let pool = generate_pool(&opts);

    let password_len = match opts.length {
        Some(n) => n,
        None => PASSWORD_LEN,
    };

    if password_len < pool.types.len() as u32 {
        bail!("Minimum password length: {}", pool.types.len());
    }

    let num: u32 = match opts.number {
        Some(n) => n,
        None => PASSWORD_NUM,
    };

    let mut counter: u32 = 0;
    while counter < num {
        let password: String = (0..password_len)
            .map(|_| {
                let index = rng.gen_range(0..pool.chars.len());
                pool.chars[index] as char
            })
            .collect();

        if is_valid(&password, &pool.types) && no_outer_spaces(&password) {
            counter += 1;
            println!("{}", password)
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn spaces() {
        let good1 = "abc123";
        let good2 = "abc 123";
        let bad1 = " abc123";
        let bad2 = " abc123 ";
        let bad3 = "abc123 ";

        assert!(no_outer_spaces(good1));
        assert!(no_outer_spaces(good2));
        assert!(!no_outer_spaces(bad1));
        assert!(!no_outer_spaces(bad2));
        assert!(!no_outer_spaces(bad3));
    }
}
